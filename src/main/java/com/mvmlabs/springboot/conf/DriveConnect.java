
package com.mvmlabs.springboot.conf;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.Drive.Files;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;

@Component
public class DriveConnect {

	private Drive drive;
	private String serviceAccountFile;
	
	private JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();

	@Autowired
	public DriveConnect(
		@Value("${googledrive.serviceaccount.json}") String serviceAccountFile) 
		throws Exception {

		this.serviceAccountFile = serviceAccountFile;

		Credential credential = serviceAccountAuthorize();
		
		String APPLICATION_NAME = "";
		
		HttpTransport httpTransport =
			GoogleNetHttpTransport.newTrustedTransport();
		
		drive = new Drive.Builder(httpTransport, JSON_FACTORY, credential)
			.setApplicationName(APPLICATION_NAME).build();

		_log.info(serviceAccountFile);
	}

	protected Credential serviceAccountAuthorize()
		throws Exception {

		GoogleCredential credential = GoogleCredential
			.fromStream(
				DriveConnect.class.getResourceAsStream(serviceAccountFile))
			.createScoped(Arrays.asList(DriveScopes.DRIVE));

		_log.info(credential.getAccessToken());

		return credential;
	}

	public List<File> retrieveAllFiles()
		throws IOException {

		List<File> result = new ArrayList<File>();

		Files.List request = drive.files().list();

		do {

			try {
				FileList files = request.execute();

				result.addAll(files.getItems());

				request.setPageToken(files.getNextPageToken());
			}
			catch (IOException e) {
				System.out.println("An error occurred: " + e);
				request.setPageToken(null);
			}

		}
		while (request.getPageToken() != null &&
			request.getPageToken().length() > 0);

		return result;
	}

	private static final Logger _log =
		LoggerFactory.getLogger(DriveConnect.class);
}
