
package com.mvmlabs.springboot.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.api.services.drive.model.File;
import com.mvmlabs.springboot.conf.DriveConnect;

@Controller
public class ItemController {

	@Autowired
	private DriveConnect drive;
	
	@RequestMapping("/api/files/all")
	@ResponseBody
	public List<File> fetchFilesAll() throws Exception {

		return drive.retrieveAllFiles();
	}
}
