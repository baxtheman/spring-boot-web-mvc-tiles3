package com.mvmlabs.springboot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by Spring Source Tool Suite.
 * 
 * @author Mark Meany
 */
@Configuration
@ComponentScan
@EnableAutoConfiguration
public class Application {

	public static void main(String[] args) {
		_log.info("start");
		
		SpringApplication.run(Application.class, args);
	}
	
	private static final Logger _log = LoggerFactory.getLogger(Application.class);
}
