
<div id="app">
	<button class="btn btn-primary" v-on:click='loadFilesAll'>Load</button>

	<ul>
		<li v-for="file in files">
			{{ file.title }}
			{{ file.fileSize }}
		</li>
	</ul>	
</div>

<script type="text/javascript">
const app4 = new Vue({
	el: '#app',
	data: {
		files: []
	},
	methods: {
		loadFilesAll :function() {
			
			this.$http.get('/api/files/all').then(response => {

				this.files = response.body;
			});			
		}
	}
})
</script>
